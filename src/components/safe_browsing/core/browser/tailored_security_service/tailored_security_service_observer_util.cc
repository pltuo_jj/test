// Copyright 2022 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "components/safe_browsing/core/browser/tailored_security_service/tailored_security_service_observer_util.h"

#include "components/prefs/pref_service.h"
#include "components/safe_browsing/core/browser/tailored_security_service/tailored_security_service.h"
#include "components/safe_browsing/core/common/safe_browsing_policy_handler.h"
#include "components/signin/public/identity_manager/identity_manager.h"
#include "url/gurl.h"

namespace safe_browsing {

const int kThresholdForInFlowNotificationMinutes = 5;

bool CanQueryTailoredSecurityForUrl(GURL url) {
  return false;
}

bool CanShowUnconsentedTailoredSecurityDialog(
    signin::IdentityManager* identity_manager,
    PrefService* prefs) {
    return false;
}

}  // namespace safe_browsing