// Copyright 2014 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/**
 * @fileoverview Externs for |templateData| global.
 * @externs
 */

/** @type {!Object|undefined} */
let templateData;
