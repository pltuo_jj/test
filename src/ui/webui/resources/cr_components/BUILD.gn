# Copyright 2018 The Chromium Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import("//build/config/chromeos/ui_mode.gni")
import("//third_party/closure_compiler/compile_js.gni")
import("//tools/grit/preprocess_if_expr.gni")
import("//tools/polymer/html_to_wrapper.gni")
import("//ui/webui/resources/tools/generate_grd.gni")
import("//ui/webui/webui_features.gni")

assert(!is_android && !is_ios)

preprocess_folder =
    "$root_gen_dir/ui/webui/resources/preprocessed/cr_components"

if (is_chromeos_ash) {
  preprocess_gen_manifest = "preprocessed_gen_manifest.json"
  preprocess_src_manifest = "preprocessed_src_manifest.json"

  generate_grd("build_grdp") {
    grd_prefix = "cr_components"
    out_grd = "$target_gen_dir/${grd_prefix}_resources.grdp"
    public_deps = [ ":preprocess" ]
    input_files_base_dir = rebase_path(".", "//")
    input_files = [
      "chromeos/cellular_setup/sim_detect_error.svg",
      "chromeos/cellular_setup/sim_detect_error_dark.svg",
      "chromeos/cellular_setup/error.svg",
      "chromeos/cellular_setup/error_dark.svg",
      "chromeos/cellular_setup/final_page_success.svg",
      "chromeos/cellular_setup/final_page_success_dark.svg",
      "chromeos/cellular_setup/default_esim_profile.svg",
      "chromeos/cellular_setup/default_esim_profile_dark.svg",
      "chromeos/network/cellular_0_with_x.svg",
      "chromeos/network/cellular_0.svg",
      "chromeos/network/cellular_1.svg",
      "chromeos/network/cellular_2.svg",
      "chromeos/network/cellular_3.svg",
      "chromeos/network/cellular_4.svg",
      "chromeos/network/cellular_off.svg",
      "chromeos/network/cellular_locked.svg",
      "chromeos/network/ethernet.svg",
      "chromeos/network/roaming_badge.svg",
      "chromeos/network/vpn.svg",
      "chromeos/network/wifi_0_with_x.svg",
      "chromeos/network/wifi_0.svg",
      "chromeos/network/wifi_1.svg",
      "chromeos/network/wifi_2.svg",
      "chromeos/network/wifi_3.svg",
      "chromeos/network/wifi_4.svg",
      "chromeos/network/wifi_off.svg",
      "chromeos/network_health/test_canceled.png",
      "chromeos/network_health/test_failed.png",
      "chromeos/network_health/test_not_run.png",
      "chromeos/network_health/test_passed.png",
      "chromeos/network_health/test_warning.png",
    ]

    manifest_files = [
      "$target_gen_dir/$preprocess_gen_manifest",
      "$target_gen_dir/$preprocess_src_manifest",
    ]

    resource_path_prefix = "cr_components"
  }
}

group("preprocess") {
  public_deps = [
    ":preprocess_generated_ts",
    ":preprocess_src_ts",
  ]

  if (is_chromeos_ash) {
    public_deps += [
      ":preprocess_generated",
      ":preprocess_src",
    ]
  }
}

if (is_chromeos_ash) {
  preprocess_if_expr("preprocess_src") {
    in_folder = "."
    out_folder = preprocess_folder
    out_manifest = "$target_gen_dir/$preprocess_src_manifest"
    in_files = [
      "chromeos/bluetooth/bluetooth_utils.js",
      "chromeos/bluetooth/bluetooth_types.js",
      "chromeos/bluetooth/bluetooth_metrics_utils.js",
      "chromeos/bluetooth/cros_bluetooth_config.js",
      "chromeos/cellular_setup/cellular_types.js",
      "chromeos/cellular_setup/cellular_setup_delegate.js",
      "chromeos/cellular_setup/esim_manager_listener_behavior.js",
      "chromeos/cellular_setup/esim_manager_utils.js",
      "chromeos/cellular_setup/mojo_interface_provider.js",
      "chromeos/cellular_setup/subflow_behavior.js",
      "chromeos/cellular_setup/webview_post_util.js",
      "chromeos/network/cellular_utils.js",
      "chromeos/network/cr_policy_network_behavior_mojo.js",
      "chromeos/network/mojo_interface_provider.js",
      "chromeos/network/network_config_element_behavior.js",
      "chromeos/network/network_listener_behavior.js",
      "chromeos/network/network_list_types.js",
      "chromeos/network/onc_mojo.js",
      "chromeos/network_health/mojo_interface_provider.js",
      "chromeos/network_health/network_diagnostics_types.js",
      "chromeos/quick_unlock/lock_screen_constants.js",
      "chromeos/smb_shares/smb_browser_proxy.js",
      "chromeos/traffic_counters/traffic_counters_adapter.js",
    ]
  }

  preprocess_if_expr("preprocess_generated") {
    deps = [ "chromeos:web_components" ]
    in_folder = target_gen_dir
    out_folder = preprocess_folder
    out_manifest = "$target_gen_dir/$preprocess_gen_manifest"

    in_files = [
      "chromeos/bluetooth/bluetooth_base_page.js",
      "chromeos/bluetooth/bluetooth_dialog.js",
      "chromeos/bluetooth/bluetooth_icon.js",
      "chromeos/bluetooth/bluetooth_icons.js",
      "chromeos/bluetooth/bluetooth_spinner_page.js",
      "chromeos/bluetooth/bluetooth_pairing_device_selection_page.js",
      "chromeos/bluetooth/bluetooth_pairing_enter_code_page.js",
      "chromeos/bluetooth/bluetooth_pairing_request_code_page.js",
      "chromeos/bluetooth/bluetooth_pairing_confirm_code_page.js",
      "chromeos/bluetooth/bluetooth_pairing_device_item.js",
      "chromeos/bluetooth/bluetooth_battery_icon_percentage.js",
      "chromeos/bluetooth/bluetooth_device_battery_info.js",
      "chromeos/bluetooth/bluetooth_pairing_ui.js",
      "chromeos/cellular_setup/activation_code_page.js",
      "chromeos/cellular_setup/activation_verification_page.js",
      "chromeos/cellular_setup/base_page.js",
      "chromeos/cellular_setup/button_bar.js",
      "chromeos/cellular_setup/cellular_setup_icons.js",
      "chromeos/cellular_setup/cellular_setup.js",
      "chromeos/cellular_setup/confirmation_code_page.js",
      "chromeos/cellular_setup/esim_flow_ui.js",
      "chromeos/cellular_setup/final_page.js",
      "chromeos/cellular_setup/profile_discovery_list_item.js",
      "chromeos/cellular_setup/profile_discovery_list_page.js",
      "chromeos/cellular_setup/provisioning_page.js",
      "chromeos/cellular_setup/psim_flow_ui.js",
      "chromeos/cellular_setup/cellular_eid_dialog.js",
      "chromeos/cellular_setup/setup_loading_page.js",
      "chromeos/network_health/network_health_container.js",
      "chromeos/network_health/network_health_summary.js",
      "chromeos/network_health/network_diagnostics.js",
      "chromeos/network_health/routine_group.js",
      "chromeos/network/cr_policy_network_indicator_mojo.js",
      "chromeos/network/network_apnlist.js",
      "chromeos/network/network_choose_mobile.js",
      "chromeos/network/network_config_input.js",
      "chromeos/network/network_config.js",
      "chromeos/network/network_config_select.js",
      "chromeos/network/network_config_toggle.js",
      "chromeos/network/network_icon.js",
      "chromeos/network/network_icons.js",
      "chromeos/network/network_ip_config.js",
      "chromeos/network/network_list_item.js",
      "chromeos/network/network_list.js",
      "chromeos/network/network_nameservers.js",
      "chromeos/network/network_password_input.js",
      "chromeos/network/network_property_list_mojo.js",
      "chromeos/network/network_proxy_exclusions.js",
      "chromeos/network/network_proxy_input.js",
      "chromeos/network/network_proxy.js",
      "chromeos/network/network_select.js",
      "chromeos/network/network_shared_css.js",
      "chromeos/network/network_siminfo.js",
      "chromeos/network/sim_lock_dialogs.js",
      "chromeos/quick_unlock/pin_keyboard_icon.js",
      "chromeos/quick_unlock/pin_keyboard.js",
      "chromeos/quick_unlock/setup_pin_keyboard.js",
      "chromeos/smb_shares/add_smb_share_dialog.js",
      "chromeos/traffic_counters/traffic_counters.js",
    ]
  }
}

# TS files are passed to a separate target so that they are not listed in the
# |out_manifest|.
preprocess_if_expr("preprocess_src_ts") {
  in_folder = "."
  out_folder = preprocess_folder
  in_files = [
    "localized_link/localized_link.ts",
    "managed_dialog/managed_dialog.ts",
    "managed_footnote/managed_footnote.ts",
  ]
}

# TS files are passed to a separate target so that they are not listed in the
# |out_manifest|.
preprocess_if_expr("preprocess_generated_ts") {
  deps = [ ":html_wrapper_files" ]
  in_folder = target_gen_dir
  out_folder = preprocess_folder
  in_files = [
    "localized_link/localized_link.html.ts",
    "managed_dialog/managed_dialog.html.ts",
    "managed_footnote/managed_footnote.html.ts",
  ]
}

html_to_wrapper("html_wrapper_files") {
  in_files = [
    "localized_link/localized_link.html",
    "managed_dialog/managed_dialog.html",
    "managed_footnote/managed_footnote.html",
  ]
}
